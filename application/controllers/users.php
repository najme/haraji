<?php 

/*
  ****************************************************************************
  ***                                                                      ***
  ***      BIDONN 1.0                                                      ***
  ***      File:  users.php                                                ***
  ***      Built: Mon June 11 15:27:24 2012                                ***
  ***      http://www.maventricks.com                                      ***
  ***                                                                      ***
  ****************************************************************************
  
   <Bidonn>
    Copyright (C) <2012> <Maventricks Technologies>.
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	If you want more information, please email me at sathick@maventricks.com or 
    contact us from http://www.maventricks.com/contactus
*/

class Users extends CI_Controller {

	//Global variable
    public $outputData;	
	public $loggedInUser;
	   
	
	 // Constructor 
	 
	function __construct()
	{
	   parent::__construct();
	    
	   	$this->load->library('settings');
		
        //Get Config Details From Db
		$this->settings->db_config_fetch();
	   
	   //Manage site Status 
		if($this->config->item('site_status') == 1)
		redirect('offline');
	  
 		
		//Load Models
		$this->load->model('common_model');
		$this->load->model('auth_model');
		$this->load->model('skills_model');
		
		//Page Title and Meta Tags
		$this->outputData = $this->common_model->getPageTitleAndMetaData();
		
		//Currency Type
		$this->outputData['currency'] = $this->db->get_where('settings', array('code' => 'CURRENCY_TYPE'))->row()->string_value;
		//$this->outputData['currency'] = $this->db->get_where('currency', array('currency_type' => $currency_type))->row()->currency_symbol;
		
		//Get Logged In user
		$this->loggedInUser					= $this->common_model->getLoggedInUser();
		$this->outputData['loggedInUser'] 	= $this->loggedInUser;
		
		//Get Footer content
		$conditions = array('page.is_active'=> 1);
		$this->outputData['pages']	=	$this->page_model->getPages($conditions);
		
		//Get Latest jobs
		$limit_latest = $this->config->item('latest_projects_limit');
		$limit3 = array($limit_latest);
		$this->outputData['latestJobs']	= $this->skills_model->getLatestJobs($limit3);
		
		//language file
		$this->lang->load('enduser/common', $this->config->item('language_code'));
		$this->lang->load('enduser/employee', $this->config->item('language_code'));
	} //Controller End 
	// --------------------------------------------------------------------
	
	/**
	 * Loads Home page of the site.
	 *
	 * @access	public
	 * @param	nil
	 * @return	void
	 */	
	function login()
	{	
		//language file
		$this->lang->load('enduser/register', $this->config->item('language_code'));
		
		//Load Models - for this function
		$this->load->model('user_model');
		
		//load validation libraray
		$this->load->library('form_validation');
		
		//Load cookie 
		$this->load->helper('cookie');
		
		//Load Form Helper
		$this->load->helper('form');
		
		//Load Library File
		$this->load->library('encrypt');
		
		// check the login for remember user 
		if($this->auth_model->getUserCookie('user_name')!='' and $this->auth_model->getUserCookie('user_password')!='')
			 { 
				 
				 //$conditions  =  array('user_name'=>$this->auth_model->getUserCookie('user_name'),'password' => md5($this->auth_model->getUserCookie('user_password')),'users.user_status' => '1');


				$conditions  =  array('user_name'=>$this->auth_model->getUserCookie('user_name'),'password' =>hash('sha384',$this->auth_model->getUserCookie('user_password')),'users.user_status' => '1');
				 $query		= $this->user_model->getUsers($conditions);
				
				if($query->num_rows() > 0)
				{
				 $this->session->set_flashdata('flash_message', $this->common_model->flash_message('success','Logged In Successfull'));
				}
				redirect('account');
			}
				
		
			if($this->uri->segment(3,0))
			{
				if($this->uri->segment(3,0)=='support')
				{
					 $this->session->set_userdata('support','support');  
				}	
				elseif($this->uri->segment(3,0)=='project')	 
				{
					 $this->session->set_userdata('job','project');  
					 $this->session->set_userdata('job_view','view');  
					 $this->session->set_userdata('job_id',$this->uri->segment(5,0));  
				   	 
				}
			}
		//Intialize values for library and helpers	
		$this->form_validation->set_error_delimiters($this->config->item('field_error_start_tag'), $this->config->item('field_error_end_tag'));
		//pr($_POST);
		//Get Form Data	
		if($this->input->post('usersLogin'))
		{
			//Set rules
			$this->form_validation->set_rules('username','lang:user_name_validation','required|trim|min_length[5]|xss_clean');
			$this->form_validation->set_rules('pwd','lang:password_validation','required|trim|xss_clean');
			if($this->form_validation->run())
			{
				if(getBanStatus($this->input->post('username')))
				{
					$this->session->set_flashdata('flash_message', $this->common_model->flash_message('error',$this->lang->line('Ban Error')));
					redirect('information');
		 		}
			
  			 	$conditions  =  array('user_name'=>$this->input->post('username'),'password' => hash('sha384',$this->input->post('pwd')),'users.user_status' => '1');
				
 				$query		 = $this->user_model->getUsers($conditions);
 
				if($query->num_rows() > 0)
				{
					  $row =  $query->row();
                      
					  // update the last activity in the users table
					  $updateData = array();
                      $updateData['last_activity'] = get_est_time();
					  //Get Activation Key
		              $activation_key = $row->id;
				      // update process for users table
				      $this->user_model->updateUser(array('id'=>$row->id),$updateData);
					  //Check For Password
					  //if($this->input->post('pwd')==$this->common_model->getDecryptedString($row->password))
					
					
					 if(1)
					 {
						//pr($row);
					 	//Set Session For User
						$this->auth_model->setUserSession($row);
						
 						if($this->input->post('remember'))
						{
						    $insertData=array();
						    $insertData['username']=$this->input->post('username');
						    $insertData['password']=$this->input->post('pwd');
						    $expire=60*60*24*100;
							if( $this->auth_model->getUserCookie('uname')=='')
							{ 
							$this->user_model->addRemerberme($insertData,$expire); 
 							}		
						 }
						 else
						 {
							   $this->user_model->removeRemeberme(); 
						 }	
						
					 	 //Notification message
						 $this->session->set_flashdata('flash_message', $this->common_model->flash_message('success','Logged In Successfull'));
						 
					   if($this->session->userdata('job_id')!='')
						{
							$jobid=$this->session->userdata('job_id');
							$this->session->unset_userdata('job');
							$this->session->unset_userdata('view');		
							$this->session->unset_userdata('jobid');	
							redirect('job/view/'.$jobid);		
						
						}
					// check for private job user login 	
					if($this->session->userdata('private_user')!='')
					{
						if($this->session->userdata('private_user')==$row->id or $this->session->userdata('creator_id')==$row->id )
						{
							 $project_id=$this->session->userdata('project_id');
							  $this->session->unset_userdata('private');
							  $this->session->unset_userdata('type');		
							  $this->session->unset_userdata('private_user');
							  $this->session->unset_userdata('project_id');	
							  $this->session->unset_userdata('creator_id');	
							 
							  redirect('job/view/'.$project_id);			
						 
						}
						else
						{
						  $this->session->unset_userdata('private');
						  $this->session->unset_userdata('type');		
						  $this->session->unset_userdata('private_user');
						  $this->session->unset_userdata('project_id');
						  $this->session->unset_userdata('creator_id');		
						  $this->session->set_flashdata('flash_message', $this->common_model->flash_message('error',$this->lang->line('This is not your private job')));
						  redirect('information');
						}
				     }	
						   
				if($this->session->userdata('support')=='' and $this->session->userdata('project')=='')
				{	
					redirect('account');	
				}
				elseif($this->session->userdata('support')!='')
				{
					$this->session->unset_userdata('support');
					redirect('support');	
				} 
				elseif($this->session->userdata('project')!='')
				{
					$id=$this->session->userdata('id');
					$this->session->unset_userdata('project');
					$this->session->unset_userdata('view');		
					$this->session->unset_userdata('id');	
					
					redirect('job/view/'.$id);				
				}
							
				 } else {

					 //Notification message
					 $this->session->set_flashdata('flash_message', $this->common_model->flash_message('error','Login failed! Incorrect username or password'));
					 redirect('users/login');
				 }
					 
				} else {
				
					 //Notification message
					 $this->session->set_flashdata('flash_message', $this->common_model->flash_message('error','Login failed! Incorrect username or password'));
				 	 redirect('users/login');
				} //If username exists			
			}//If End - Check For Validation				
		} //If End - Check For Form Submission
		$this->load->view('members/view_login',$this->outputData);
	} //Function login End
	// --------------------------------------------------------------------
	
	/**
	 * Loads forgotPassword page of the site.
	 *
	 * @access	public
	 * @param	nil
	 * @return	void
	 */	
	function forgotPassword()
	{	
		//language file
		$this->lang->load('enduser/forgot', $this->config->item('language_code'));
		
		//Load Models - for this function
		$this->load->model('user_model');
		
		//load validation libraray
		$this->load->library('form_validation');
		
		//Load Form Helper
		$this->load->helper('form');
		
		//Intialize values for library and helpers	
		$this->form_validation->set_error_delimiters($this->config->item('field_error_start_tag'), $this->config->item('field_error_end_tag'));
		
		//Get Form Data	- Forgot Password
		if($this->input->post('forgotPassword'))
		{
			//Set rules
			$this->form_validation->set_rules('username','lang:user_name_validation','required|trim|min_length[5]|xss_clean');			
			if($this->form_validation->run())
			{
				$username 		=	$this->input->post('username');
				$conditions 	= 	array('user_name'=>$username);
				$query 			=  	$this->user_model->getUsers($conditions);
				$usersData      =   $query->row();
				
				if($query->num_rows()>0)
				{
				 $newpassword    =   '';
				 for($i=0;$i<5;$i++)
				  {
				  $newpassword .=chr(rand(65,90));
				  $newpassword .=chr(rand(97,122));
				  }
				 //Update the suers password	
				 //$updateData['password']    		  = md5($newpassword);
				 
				 $updateData['password'] = hash('sha384',$newpassword);
				 $updateKey = array('users.id'=>$usersData->id);
			     $this->user_model->updateUser($updateKey,$updateData);
					
				//Send Mail
				$conditionUserMail = array('email_templates.type'=>'forget_password');
				$this->load->model('email_model');
				$result            = $this->email_model->getEmailSettings($conditionUserMail);
				$rowUserMailConent = $result->row();
				$splVars = array("!site_title" => $this->config->item('site_title'), "!url" => site_url(''), "!username" =>$usersData->user_name ,"!newpassword" =>$newpassword);
				
				$mailSubject = $rowUserMailConent->mail_subject;
				$mailContent = strtr($rowUserMailConent->mail_body, $splVars);		
				$toEmail = $usersData->email;
				$fromEmail = $this->config->item('site_admin_mail');
				$this->email_model->sendHtmlMail($toEmail,$fromEmail,$mailSubject,$mailContent);	

				//Notification message
				$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success','Your password has been sent to your registered email address!'));
				redirect('users/forgotPassword');					
				} else {
					 //Notification message
					 $this->session->set_flashdata('flash_message', $this->common_model->flash_message('error','User Name Failed'));
				 	 redirect('users/forgotPassword');					
				}
			}//If End - Check For Validation				
		} //If End - Check For Form Submission For Forgot Password
		
		//Get Form Data	- Forgot Username
		if($this->input->post('forgotUsername'))
		{
			//Set rules
			$this->form_validation->set_rules('email','lang:email_validation','required|trim|valid_email|xss_clean');			
			if($this->form_validation->run())
			{
				$email 			=	$this->input->post('email');
				$conditions 	= 	array('email'=>$email);
				$query 			=  	$this->user_model->getUsers($conditions);
				$usersData      =   $query->row();
				if($query->num_rows()>0)
				{
				 //Create new password
				 $newpassword    =   '';
				 for($i=0;$i<5;$i++)
				  {
				  $newpassword .=chr(rand(65,90));
				  $newpassword .=chr(rand(97,122));
				  }
				 //Update the suers password	
				 //$updateData['password']    		  = md5($newpassword);
				 $updateData['password'] = hash('sha384',$newpassword);
				 $updateKey 		= array('users.id'=>$usersData->id);
			     $this->user_model->updateUser($updateKey,$updateData);
					
				//Send Mail
				$conditionUserMail = array('email_templates.type'=>'forget_password');
				$this->load->model('email_model');
				$result            = $this->email_model->getEmailSettings($conditionUserMail);
				$rowUserMailConent = $result->row();
				$splVars = array("!site_title" => $this->config->item('site_title'), "!url" => site_url(''), "!username" =>$usersData->user_name ,"!newpassword" =>$newpassword);
				$mailSubject = $rowUserMailConent->mail_subject;
				$mailContent = strtr($rowUserMailConent->mail_body, $splVars);		
				$toEmail = $usersData->email;
				$fromEmail = $this->config->item('site_admin_mail');
				$this->email_model->sendHtmlMail($toEmail,$fromEmail,$mailSubject,$mailContent);	
					
				//Notification message
				$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success','Your username has been sent to your registered email address'));
				redirect('users/forgotPassword');					
				} else {
					 //Notification message
					 $this->session->set_flashdata('flash_message', $this->common_model->flash_message('error','Email Failed'));
				 	 redirect('users/forgotPassword');					
				}				
			}//If End - Check For Validation				
		} //If End - Check For Form Submission For Forgot Username	
		$this->load->view('members/view_forgot',$this->outputData);
	} //Function forgotPassword End
	// --------------------------------------------------------------------
	
	/**
	 * Loads logout .
	 *
	 * @access	public
	 * @param	nil
	 * @return	void
	 */	
	function logout()
	{	
		$this->auth_model->clearUserSession();
		$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
		$this->auth_model->clearUserCookie(array('username','password'));
		$this->auth_model->clearUserCookie(array('user_name','user_password'));
		redirect('information');
				
	} //Function logout End
	
	// --------------------------------------------------------------------
	
	/**
	 * Loads login for user to post  .
	 *
	 * @access	public
	 * @param	nil
	 * @return	void
	 */	
	function post()
	{	
		$this->auth_model->clearUserSession();
		$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
		redirect('information');
	} //Function logout End
	
	/**
	 *Get the  job deatils for session check 
	 *
	 * @access	public
	 * @param	nil
	 * @return	void
	 */	
	
	function getData()
	{ 
	//language file
		$this->lang->load('enduser/loginUsers', $this->config->item('language_code'));
		
		//Load Models - for this function
		$this->load->model('user_model');
		
		//load validation libraray
		$this->load->library('form_validation');
		
		//Load cookie 
		$this->load->helper('cookie');
		
		//Load Form Helper
		$this->load->helper('form');
		
		//Load Library File
		$this->load->library('encrypt');
		$this->uri->segment(4);
		
		if($this->uri->segment(3)!='')
			{
			 
					 $this->session->set_userdata('project','project');  
					 $this->session->set_userdata('view','view');  
					 $this->session->set_userdata('id',$this->uri->segment(3,0));  
					 
				
			}
			redirect('users/login');
	}//Function getData End
	//---------------------------------------------------------------------------------
	
	/**
	 * get the details from private job to store session
	 *
	 * @access	public
	 * @param	nil
	 * @return	void
	 */	
	function getProjectDetails()
	{ 
	    //language file
		$this->lang->load('enduser/register', $this->config->item('language_code'));
		
		//Load Models - for this function
		$this->load->model('user_model');
		
		
		//load validation libraray
		$this->load->library('form_validation');
		
		//Load cookie 
		$this->load->helper('cookie');
		
		//Load Form Helper
		$this->load->helper('form');
		
		//Load Library File
		$this->load->library('encrypt');
		$this->uri->segment(4);
		
		if($this->uri->segment(3)!='')
			{
			 
				$this->session->set_userdata('private','project');  
				$this->session->set_userdata('type','view');  
				$this->session->set_userdata('project_id',$this->uri->segment(3,0)); 
				$this->session->set_userdata('private_user',$this->uri->segment(4,0)); 
				$this->session->set_userdata('creator_id',$this->uri->segment(5,0));
				$condition='jobs.id='.$this->uri->segment(3,0);
				$query="SELECT * FROM jobs WHERE ". $condition;
				$result=$this->db->query($query);
					foreach( $result->result() as $project)
					{
						$project_name=$project->job_name;
					}
				redirect('users/login/'.$project_name);
			}
			
	}//Function getProjectDetails End 
	//---------------------------------------------------------------------------- 
	
	
	function load_users()
	{
		if($this->input->post('type_id'))
		{
 		$user_id     =$this->loggedInUser->id;  	
		
		//Get logged user role
		$role   =  $this->loggedInUser->role_id;
		$project_id = $this->input->post('type_id');
		
		//Get the users details
	    $condition    = array('jobs.id'=>$project_id);	
		$usersJob	   =  $this->skills_model->getMembersJob($condition);
		$this->outputData['usersProject'] =  $usersJob->result();	
		foreach($usersJob->result() as $res)
		  {
		    
		  	if($role == '1')
			   $userid = $res->employee_id;
			if($role == '2')
			   $userid = $res->creator_id;
		  }
		
		//Get the users details
	    $this->load->model('user_model');
		$condition    = array('users.id'=>$userid);	
		$usersname	   =  $this->user_model->userProjectdata($condition);
		$this->outputData['usersname'] =  $usersname->result();	
		if($usersname)
		{
			foreach($usersname->result() as $users)
			{
			?>
          <option value="<?php echo $users->id; ?>"> <?php echo $users->user_name; ?></option>
          <?php				
			}
		} 
		?>
			<?php
		} else {
			if($this->input->post('type_id') == '0')
			  {
				//Get logged user role
				   $this->outputData['logged_userrole']   =  $this->loggedInUser->role_id;
				   $role                                  =  $this->loggedInUser->role_id;
				   ?>
				<select id='users_load' name="users_load">  
				<?php 
				if($role == '1')
				{ ?>
					<option value="0"> <?php echo '<b>-- '.$this->lang->line('Select Employee').' --</b>'; ?></option>	<?php 
				}
				if($role == '2')
				{ ?>
					<option value="0"> <?php echo '<b>-- '.$this->lang->line('Select Owner').' --</b>'; ?></option>	<?php  
				}
				  
			  } 
		}
		exit;
	} //Function Load_user
	
	
	
	function load_users1()
	{
	//pr($this->uri->segment_array());exit;
		if($this->input->post('type_id') or $this->uri->segment(3))
		{
		//Here the code for select if user choosed in job combo box
		$user_id     =$this->loggedInUser->id;  	
		
		//Get logged user role
		$role   =  $this->loggedInUser->role_id;
		$project_id = $this->uri->segment(3);		
		
		//Get the users details
	    if($this->uri->segment(3))
		$condition    = array('jobs.id'=>$project_id);
		$usersJob	   =  $this->skills_model->getMembersJob($condition);
		$this->outputData['usersProject'] =  $usersJob->result();	
		foreach($usersJob->result() as $res)
		  {
		    
		  	if($role == '1')
			   $userid = $res->employee_id;
			if($role == '2')
			   $userid = $res->creator_id;
		  }
		
		//Get the users details
	    $this->load->model('user_model');
		$condition    = array('users.id'=>$userid);	
		$usersname	  =  $this->user_model->userProjectdata($condition);
		$this->outputData['usersname'] =  $usersname->result();	
		$default='Everyone';
		?><?php 
		if($usersname)
		{
			$data='<select name="prog_id">';
			foreach($usersname->result() as $users)
			{
			    
				$data.= '<option value="'.$users->id.'">'.$users->user_name.'</option>';
				         		
			}
			$data.='<option value="0">'.$default.'</option>';
			$data.='</select>';
			echo $data;
		} 
		?>
			<?php
		} else {
			if($this->input->post('type_id') == '0')
			  {
				
				//Get logged user role
				$this->outputData['logged_userrole']   =  $this->loggedInUser->role_id;
				$role                                  =  $this->loggedInUser->role_id;
				echo 'Please Choose Job';
				  
			  } 
		} 
		exit;
	} //Function Load_Category
	
	function load_users2()
	{
	//pr($this->uri->segment_array());exit;
		if($this->input->post('type_id') or $this->uri->segment(3))
		{
		//Here the code for select if user choosed in job combo box
		$user_id     =$this->loggedInUser->id;  	
		
		//Get logged user role
		$role   =  $this->loggedInUser->role_id;
		$project_id = $this->uri->segment(3);		
		
		//Get the users details
	    if($this->uri->segment(3))
		   $condition    = array('job.id'=>$project_id);	
		$usersJob	   =  $this->skills_model->getMembersJob($condition);
		$this->outputData['usersProject'] =  $usersJob->result();	
		foreach($usersJob->result() as $res)
		  {
		    
		  	if($role == '1')
			   $userid = $res->employee_id;
			if($role == '2')
			   $userid = $res->creator_id;
		  }
		
		//Get the users details
	    $this->load->model('user_model');
		$condition    = array('users.id'=>$userid);	
		$usersname	   =  $this->user_model->userProjectdata($condition);
		$this->outputData['usersname'] =  $usersname->result();	
		$default='No Owner';
		?><?php 
		if($usersname)
		{
			$data='<select name="prog_id">';
			
			foreach($usersname->result() as $users)
			{
			   if($users->user_name!='')
			   {
			   $data.= '<option value="'.$users->id.'">'.$users->user_name.'</option>';
			   }
			    else
				{
				 $data.='<option value="0" selected="selected">'.$default.'</option>';
				}
  			}
			//$data.='<option value="0">'.$default.'</option>';
			$data.='</select>';
			echo $data;
		} 
		?>
		<?php
		} else {
			if($this->input->post('type_id') == '0')
			  {
				
				//Get logged user role
				$this->outputData['logged_userrole']   =  $this->loggedInUser->role_id;
				$role                                  =  $this->loggedInUser->role_id;
				echo 'Please Choose Job';
 			  } 
		} 
		exit;
	} //Function Load_Category
	
	
} //End  Users Class

/* End of file Users.php */ 
/* Location: ./application/controllers/Users.php */
?>