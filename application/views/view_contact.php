<?php $this->load->view('header'); ?>
<div class="clsMinContent clearfix">
<?php $this->load->view('sidebar'); ?>
<!--START MAIN-->
<div id="main" style="border:1px solid #ccc;">
  <!--POST JOB-->
  <div class="clsContact">
    
                        <div class="clsInnerCommon clsSitelinks">
                          <h2><?php echo $this->lang->line('contacting'); ?> <?php echo $this->config->item('site_title'); ?></h2>
                          <div class="clsContactForm clSTextDec">
                            <h3><span class="clsCategory"><?php echo $this->lang->line('Contact');?></span></h3>
                              <form method="post" action="<?php echo site_url('contact')?>">
       							<p>
								  <label><?php echo $this->lang->line('your_email'); ?><span class="red">*</span></label>
								  <input class="clsText" type="text" name="c_email" value="<?php echo set_value('c_email'); ?>" />
								  <?php echo form_error('c_email'); ?>
								</p>
								<p>
								  <label><?php echo $this->lang->line('subject'); ?><span class="red">*</span> </label>
								  <input class="clsText" type="text" name="c_subject" value="<?php echo set_value('c_subject'); ?>" />
								  <?php echo form_error('c_subject'); ?>
								</p>
								<p>
								  <label class="clsComments"><?php echo $this->lang->line('comments'); ?><span class="red">*</span></label>
								  <textarea name="c_comments" rows="10" cols="40"><?php echo set_value('c_comments'); ?></textarea>
								  <?php echo form_error('c_comments'); ?>
								</p>
								<p class="clsSubmitBlock">
								  <label>&nbsp;</label>
								  <input type="submit" value="<?php echo $this->lang->line('Submit');?>" name="postContact" class="clsLogin_but" />
								  <!--<input type="image" src="<?php echo image_url('bt_sbmitmsg.jpg');?>"/>-->
								</p>
							</form>
                            <p>
                              <label>&nbsp;</label>
                              <b><?php echo $this->lang->line('note');?> <a href="<?php echo site_url('users/login');?>"><?php echo $this->lang->line('login here');?></a>.</b></p>
                          </div>
                        </div>
                      </div>

  <!--END OF POST JOB-->
</div>
</div></div>
<!--END OF MAIN-->
<?php $this->load->view('footer'); ?>