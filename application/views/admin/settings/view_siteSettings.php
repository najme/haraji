<?php $this->load->view('admin/header'); ?>
<?php $this->load->view('admin/sidebar'); ?>

<div id="main">
  <div class="clsSettings">
    <div class="clsMainSettings">
 	 <div class="inner_t">
      <div class="inner_r">
        <div class="inner_b">
          <div class="inner_l">
            <div class="inner_tl">
              <div class="inner_tr">
                <div class="inner_bl">
                  <div class="inner_br">   
	 
				  <?php
						//Show Flash Message
						if($msg = $this->session->flashdata('flash_message'))
						{
							echo $msg;
						}
					?>
      <div class="clsNav">
		<ul>
			<li class="clsNoBorder"><a href="<?php echo admin_url('siteSettings/dbBackup')?>"><?php echo $this->lang->line('db_backup');?></a></li>
		</ul>
      </div>
	  <div class="clsTitle">
	   <h3 align="left"><?php echo $this->lang->line('website_settings'); ?></h3>
	   </div>
	  
		 <form action="<?php echo admin_url('siteSettings'); ?>" method="post" enctype="multipart/form-data">
		 <table class="table1" cellpadding="2" cellspacing="0">
		<tbody>
       <tr>
          <td class="clsName"><?php echo $this->lang->line('website_title'); ?><span class="clsRed">*</span></td>
          <td class="clsMailIds"><input name="site_title" type="text" class="clsTextBox" value="<?php if(isset($settings['SITE_TITLE'])) echo $settings['SITE_TITLE']; ?>" />
          <?php echo form_error('site_title'); ?></td></tr>
       <tr>
         <td><?php echo $this->lang->line('website_slogan'); ?><span class="clsRed">*</span></td>
          <td><input name="site_slogan" type="text" class="clsTextBox" value="<?php if(isset($settings['SITE_SLOGAN'])) echo $settings['SITE_SLOGAN']; ?>" />
          <?php echo form_error('site_slogan'); ?></td></tr>
		  <tr>
         <td><?php echo $this->lang->line('site_url'); ?><span class="clsRed">*</span></td>
          <td><input name="base_url" type="text" class="clsTextBox" value="<?php if(isset($settings['BASE_URL'])) echo $settings['BASE_URL']; ?>" />
          <?php echo form_error('base_url'); ?> </td></tr>
		<tr>
          <td><?php echo $this->lang->line('website_admin_mail'); ?><span class="clsRed">*</span></td>
           <td><input name="site_admin_mail" type="text" class="clsTextBox" value="<?php if(isset($settings['SITE_ADMIN_MAIL'])) echo $settings['SITE_ADMIN_MAIL']; ?>"  />
          <?php echo form_error('site_admin_mail'); ?></td></tr>  
        <tr>
		<!-- <INPUT TYPE="hidden" NAME="site_language" value='<?php //if(isset($settings['LANGUAGE_CODE'])) echo $settings['LANGUAGE_CODE']; ?>'> -->
		<tr>
          <td><?php echo $this->lang->line('language code'); ?><span class="clsRed">*</span></td>
           <td><input name="site_language" type="text" class="clsTextBox" value="<?php if(isset($settings['LANGUAGE_CODE'])) echo $settings['LANGUAGE_CODE']; ?>" />
          <?php echo form_error('language_code'); ?></td></tr>  
        <tr> 
		
          <td><?php echo $this->lang->line('website_closed'); ?><span class="clsRed">*</span></td>
           <td><input name="site_status" type="radio" disabled="disabled"  class="clsRadioBut"  value="1" <?php if(isset($settings['SITE_STATUS']) and $settings['SITE_STATUS']==1)  echo 'checked="checked"'; ?>  />
          <?php echo $this->lang->line('On');?>
          <input type="radio" name="site_status" class="clsRadioBut"  value="0"<?php if(isset($settings['SITE_STATUS']) and $settings['SITE_STATUS']==0)  echo 'checked="checked"';   ?>  />
          <?php echo $this->lang->line('Off');?></td></tr>
        <tr>
          <td><?php echo $this->lang->line('closed_message'); ?><span class="clsRed">*</span></td>
          <td><textarea class="clsTextArea" name="offline_message"><?php if(isset($settings['OFFLINE_MESSAGE'])) echo $settings['OFFLINE_MESSAGE']; ?> 
</textarea>
          <?php echo form_error('offline_message'); ?> </td></tr>
		  
       <tr>
          <td><?php echo $this->lang->line('Forced Escrow'); ?><span class="clsRed">*</span></td>
         <td><input type="radio"  class="clsRadioBut" name="forced_escrow"  value="1"  <?php if(isset($settings['FORCED_ESCROW']) and $settings['FORCED_ESCROW']==1)  echo 'checked="checked"'; ?>/>
          <?php echo $this->lang->line('Yes');?>
          <input type="radio" name="forced_escrow" class="clsRadioBut" value="0"<?php if(isset($settings['FORCED_ESCROW']) and $settings['FORCED_ESCROW']==0)  echo 'checked="checked"'; ?>/>
          <?php echo $this->lang->line('No');?></td>
		  </tr>
		  <tr>
          <td><?php echo $this->lang->line('currency type'); ?><span class="clsRed">*</span></td>
          <td><select name="currency">
		  <?php if(isset($currency) and count($currency->num_rows)>0){ foreach($currency->result() as $curr){ ?>
		  <option value="<?php echo $curr->currency_type;?>" <?php if($settings['CURRENCY_TYPE']==$curr->currency_type) echo 'selected="selected"'; ?>><?php echo $curr->currency_name; ?></option>
		  <?php }} ?>
		  </select>
          <?php echo form_error('payment_settings'); ?></td>
		  </tr>
		   <tr>
          <td><?php echo $this->lang->line('time zone'); ?><span class="clsRed">*</span></td>
          <td>
		  <select name="timezones">
		<option value="UM12" <?php if($settings['TIME_ZONE']== 'UM12') echo 'selected="selected"'; ?>>(UTC - 12:00) Enitwetok, Kwajalien</option>
		<option value="UM11" <?php if($settings['TIME_ZONE']== 'UM11') echo 'selected="selected"'; ?>>(UTC - 11:00) Nome, Midway Island, Samoa</option>
		<option value="UM10" <?php if($settings['TIME_ZONE']== 'UM10') echo 'selected="selected"'; ?>>(UTC - 10:00) Hawaii</option>
		<option value="UM9" <?php if($settings['TIME_ZONE']== 'UM9') echo 'selected="selected"'; ?>>(UTC - 9:00) Alaska</option>
		<option value="UM8" <?php if($settings['TIME_ZONE']== 'UM8') echo 'selected="selected"'; ?>>(UTC - 8:00) Pacific Time</option>
		<option value="UM7" <?php if($settings['TIME_ZONE']== 'UM7') echo 'selected="selected"'; ?>>(UTC - 7:00) Mountain Time</option>
		<option value="UM6" <?php if($settings['TIME_ZONE']== 'UM6') echo 'selected="selected"'; ?>>(UTC - 6:00) Central Time, Mexico City</option>
		<option value="UM5" <?php if($settings['TIME_ZONE']== 'UM5') echo 'selected="selected"'; ?>>(UTC - 5:00) Eastern Time, Bogota, Lima, Quito</option>
		<option value="UM4" <?php if($settings['TIME_ZONE']== 'UM4') echo 'selected="selected"'; ?>>(UTC - 4:00) Atlantic Time, Caracas, La Paz</option>
		<option value="UM25" <?php if($settings['TIME_ZONE']== 'UM25') echo 'selected="selected"'; ?>>(UTC - 3:30) Newfoundland</option>
		<option value="UM3" <?php if($settings['TIME_ZONE']== 'UM3') echo 'selected="selected"'; ?>>(UTC - 3:00) Brazil, Buenos Aires, Georgetown, Falkland Is.</option>
		<option value="UM2" <?php if($settings['TIME_ZONE']== 'UM2') echo 'selected="selected"'; ?>>(UTC - 2:00) Mid-Atlantic, Ascention Is., St Helena</option>
		<option value="UM1" <?php if($settings['TIME_ZONE']== 'UM1') echo 'selected="selected"'; ?>>(UTC - 1:00) Azores, Cape Verde Islands</option>
		<option value="UTC" <?php if($settings['TIME_ZONE']== 'UTC') echo 'selected="selected"'; ?>>(UTC) Casablanca, Dublin, Edinburgh, London, Lisbon, Monrovia</option>
		<option value="UP1" <?php if($settings['TIME_ZONE']== 'UP1') echo 'selected="selected"'; ?>>(UTC + 1:00) Berlin, Brussels, Copenhagen, Madrid, Paris, Rome</option>
		<option value="UP2" <?php if($settings['TIME_ZONE']== 'UP2') echo 'selected="selected"'; ?>>(UTC + 2:00) Kaliningrad, South Africa, Warsaw</option>
		<option value="UP3" <?php if($settings['TIME_ZONE']== 'UP3') echo 'selected="selected"'; ?>>(UTC + 3:00) Baghdad, Riyadh, Moscow, Nairobi</option>
		<option value="UP25" <?php if($settings['TIME_ZONE']== 'UP25') echo 'selected="selected"'; ?>>(UTC + 3:30) Tehran</option>
		<option value="UP4" <?php if($settings['TIME_ZONE']== 'UP4') echo 'selected="selected"'; ?>>(UTC + 4:00) Adu Dhabi, Baku, Muscat, Tbilisi</option>
		<option value="UP35" <?php if($settings['TIME_ZONE']== 'UP35') echo 'selected="selected"'; ?>>(UTC + 4:30) Kabul</option>
		<option value="UP5" <?php if($settings['TIME_ZONE']== 'UP5') echo 'selected="selected"'; ?>>(UTC + 5:00) Islamabad, Karachi, Tashkent</option>
		<option value="UP45" <?php if($settings['TIME_ZONE']== 'UP45') echo 'selected="selected"'; ?>>(UTC + 5:30) Bombay, Calcutta, Madras, New Delhi</option>
		<option value="UP6" <?php if($settings['TIME_ZONE']== 'UP6') echo 'selected="selected"'; ?>>(UTC + 6:00) Almaty, Colomba, Dhaka</option>
		<option value="UP7" <?php if($settings['TIME_ZONE']== 'UP7') echo 'selected="selected"'; ?>>(UTC + 7:00) Bangkok, Hanoi, Jakarta</option>
		<option value="UP8" <?php if($settings['TIME_ZONE']== 'UP8') echo 'selected="selected"'; ?>>(UTC + 8:00) Beijing, Hong Kong, Perth, Singapore, Taipei</option>
		<option value="UP9" <?php if($settings['TIME_ZONE']== 'UP9') echo 'selected="selected"'; ?>>(UTC + 9:00) Osaka, Sapporo, Seoul, Tokyo, Yakutsk</option>
		<option value="UP85" <?php if($settings['TIME_ZONE']== 'UP85') echo 'selected="selected"'; ?>>(UTC + 9:30) Adelaide, Darwin</option>
		<option value="UP10" <?php if($settings['TIME_ZONE']== 'UP10') echo 'selected="selected"'; ?>>(UTC + 10:00) Melbourne, Papua New Guinea, Sydney, Vladivostok</option>
		<option value="UP11" <?php if($settings['TIME_ZONE']== 'UP11') echo 'selected="selected"'; ?>>(UTC + 11:00) Magadan, New Caledonia, Solomon Islands</option>
		<option value="UP12" <?php if($settings['TIME_ZONE']== 'UP12') echo 'selected="selected"'; ?>>(UTC + 12:00) Auckland, Wellington, Fiji, Marshall Island</option>
		</select>
          </td>
		  </tr>	
		  <tr>
          <td><?php echo $this->lang->line('daylight savings'); ?><span class="clsRed">*</span></td>
          <td>
		  <select name="daylight">
		<option value="TRUE" <?php if($settings['DAYLIGHT']== 'TRUE') echo 'selected="selected"'; ?>>TRUE</option>
		<option value="FALSE" <?php if($settings['DAYLIGHT']== 'FALSE') echo 'selected="selected"'; ?>>FALSE</option>
		</select>
          </td>
		  </tr>	
		  <tr>
          <td><?php echo $this->lang->line('min_balance'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="payment_settings" value="<?php if(isset($settings['PAYMENT_SETTINGS'])) echo $settings['PAYMENT_SETTINGS']; ?>"/>
          <?php echo form_error('payment_settings'); ?></td></tr>
       <tr>
		   <td><?php echo $this->lang->line('featured_jobs_limit'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="featured_jobs_limit" value="<?php if(isset($settings['FEATURED_PROJECTS_LIMIT'])) echo $settings['FEATURED_PROJECTS_LIMIT']; ?>"/>
          <?php echo form_error('featured_jobs_limit'); ?></td></tr>
       <tr>
          <td><?php echo $this->lang->line('urgent_jobs_limit'); ?><span class="clsRed">*</span></td>
         <td> <input class="clsTextBox" type="text" name="urgent_jobs_limit" value="<?php if(isset($settings['URGENT_PROJECTS_LIMIT'])) echo $settings['URGENT_PROJECTS_LIMIT']; ?>"/>
          <?php echo form_error('urgent_jobs_limit'); ?></td></tr>
       <tr>
          <td><?php echo $this->lang->line('latest_jobs_limit'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="latest_jobs_limit" value="<?php if(isset($settings['LATEST_PROJECTS_LIMIT'])) echo $settings['LATEST_PROJECTS_LIMIT']; ?>"/>
          <?php echo form_error('latest_jobs_limit'); ?></td></tr>
        <tr>
          <td><?php echo $this->lang->line('employee_commission_amount'); ?><span class="clsRed">*</span></td>
         <td> <input class="clsTextBox" type="text" name="employee_commission_amount" value="<?php if(isset($settings['PROVIDER_COMMISSION_AMOUNT'])) echo $settings['PROVIDER_COMMISSION_AMOUNT']; ?>"/>
          <?php echo form_error('employee_commission_amount'); ?></td></tr>
       <tr>
          <td><?php echo $this->lang->line('featured_jobs_amount'); ?><span class="clsRed">*</span></td>
         <td> <input class="clsTextBox" type="text" name="featured_jobs_amount" value="<?php if(isset($settings['FEATURED_PROJECT_AMOUNT'])) echo $settings['FEATURED_PROJECT_AMOUNT']; ?>"/>
          <?php echo form_error('featured_jobs_amount'); ?></td></tr>
      <tr>
          <td><?php echo $this->lang->line('urgent_jobs_amount'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="urgent_jobs_amount" value="<?php if(isset($settings['URGENT_PROJECT_AMOUNT'])) echo $settings['URGENT_PROJECT_AMOUNT']; ?>"/>
          <?php echo form_error('urgent_jobs_amount'); ?></td>
		 <tr>
          <td><?php echo $this->lang->line('joblist_jobs_amount'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="joblist_jobs_amount" value="<?php if(isset($settings['JOBLISTING_PROJECT_AMOUNT'])) echo $settings['JOBLISTING_PROJECT_AMOUNT']; ?>"/>
          <?php echo form_error('joblist_jobs_amount'); ?></td></tr>
		  
          <tr>
          <td><?php echo $this->lang->line('Joblist_validity_days'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="joblist_validity_days" value="<?php if(isset($settings['JOBLIST_VALIDITY_LIMIT'])) echo $settings['JOBLIST_VALIDITY_LIMIT']; ?>"/>
          <?php echo form_error('joblist_validity_days'); ?></td></tr>
		  		  
		   
       <tr>
          <td><?php echo $this->lang->line('hide_jobs_amount'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="hide_jobs_amount" value="<?php if(isset($settings['HIDE_PROJECT_AMOUNT'])) echo $settings['HIDE_PROJECT_AMOUNT']; ?>"/>
          <?php echo form_error('hide_jobs_amount'); ?></td></tr>
		  
		  
		   <tr>
          <td><?php echo $this->lang->line('private_job_amount'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="private_job_amount" value="<?php if(isset($settings['PRIVATE_PROJECT_AMOUNT'])) echo $settings['PRIVATE_PROJECT_AMOUNT']; ?>"/>
          <?php echo form_error('private_job_amount'); ?></td>
		  </tr>

		 <tr>
          <td><?php echo $this->lang->line('featured_jobs_amount for certificate member'); ?><span class="clsRed">*</span></td>
         <td> <input class="clsTextBox" type="text" name="featured_jobs_amount_cm" value="<?php if(isset($settings['FEATURED_PROJECT_AMOUNT_CM'])) echo $settings['FEATURED_PROJECT_AMOUNT_CM']; ?>"/>
          <?php echo form_error('featured_jobs_amount_cm'); ?></td></tr>
      <tr>
          <td><?php echo $this->lang->line('urgent_jobs_amount for certificate member'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="urgent_jobs_amount_cm" value="<?php if(isset($settings['URGENT_PROJECT_AMOUNT_CM'])) echo $settings['URGENT_PROJECT_AMOUNT_CM']; ?>"/>
          <?php echo form_error('urgent_jobs_amount_cm'); ?></td>
		 
       <tr>
          <td><?php echo $this->lang->line('hide_jobs_amount for certificate member'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="hide_jobs_amount_cm" value="<?php if(isset($settings['HIDE_PROJECT_AMOUNT_CM'])) echo $settings['HIDE_PROJECT_AMOUNT_CM']; ?>"/>
          <?php echo form_error('hide_jobs_amount_cm'); ?></td></tr>
		  
		  
		   <tr>
          <td><?php echo $this->lang->line('private_job_amount for certificate member'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="private_jobs_amount_cm" value="<?php if(isset($settings['PRIVATE_PROJECT_AMOUNT_CM'])) echo $settings['PRIVATE_PROJECT_AMOUNT_CM']; ?>"/>
          <?php echo form_error('private_jobs_amount_cm'); ?></td></tr> 
		  
       <tr>
          <td><?php echo $this->lang->line('file_manager_limit'); ?><span class="clsRed">*</span></td>
          <td><input class="clsTextBox" type="text" name="file_manager_limit" value="<?php if(isset($settings['USER_FILE_LIMIT'])) echo $settings['USER_FILE_LIMIT']; ?>"/>
          <?php echo form_error('file_manager_limit'); ?></td>
        <tr>
		<tr>

          <td><?php echo $this->lang->line('Twitter Username'); ?></td>

          <td><input class="clsTextBox" type="text" name="twitter_username" value="<?php if(isset($settings['TWITTER_USERNAME'])) echo $settings['TWITTER_USERNAME']; ?>"/>

          <?php echo form_error('twitter_username'); ?></td>

        <tr>

		<tr>

          <td><?php echo $this->lang->line('Twitter Password'); ?></td>

          <td><input class="clsTextBox" type="text" name="twitter_password" value="<?php if(isset($settings['TWITTER_PASSWORD'])) echo $settings['TWITTER_PASSWORD']; ?>"/>

          <?php echo form_error('twitter_password'); ?></td>

        <tr>
		<tr>

          <td><?php echo $this->lang->line('Facebook'); ?></td>

          <td><input class="clsTextBox" type="text" name="facebook" value="<?php if(isset($settings['FACEBOOK'])) echo $settings['FACEBOOK']; ?>"/>

          <?php echo form_error('facebook'); ?></td>

        <tr>
		<tr>

          <td><?php echo $this->lang->line('Twitter'); ?></td>

          <td><input class="clsTextBox" type="text" name="twitter" value="<?php if(isset($settings['TWITTER'])) echo $settings['TWITTER']; ?>"/>

          <?php echo form_error('twitter'); ?></td>

        <tr>
		<tr>

          <td><?php echo $this->lang->line('RSS'); ?></td>

          <td><input class="clsTextBox" type="text" name="rss" value="<?php if(isset($settings['RSS'])) echo $settings['RSS']; ?>"/>

          <?php echo form_error('rss'); ?></td>

        <tr>
		<tr>

          <td><?php echo $this->lang->line('LinkedIn'); ?></td>

          <td><input class="clsTextBox" type="text" name="linkedin" value="<?php if(isset($settings['LINKEDIN'])) echo $settings['LINKEDIN']; ?>"/>

          <?php echo form_error('linkedin'); ?></td>

        <tr>
        <td></td>
          <td><input class="clsSubmitBt1" value="<?php echo $this->lang->line('Submit');?>" name="siteSettings" type="submit">
        </td>
      <!--</form>-->
	  </tr>
	  </tbody></table>
	  </form>
	  </div></div></div></div></div></div></div></div>
	 
    </div>
  </div>
</div>


<?php $this->load->view('admin/footer'); ?>
